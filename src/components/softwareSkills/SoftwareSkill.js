import React from "react";
import "./SoftwareSkill.css";
import { skillsSection } from "../../portfolio";

export default function SoftwareSkill() {
  return (
    <div>
      <div className="software-skills-main-div">
      <hr></hr>
        <ul className="dev-icons">
          {skillsSection.softwareSkills.map(skills => {
            return (
              <li className="software-skill-inline" name={skills.skillName}>
                <i className={skills.fontAwesomeClassname}></i>
                <p>{skills.skillName}</p>
              </li>
            );
          })}
        </ul>
      <div className="mx-auto">
         <li className="badge badge-pill my-0 mx-1 badge-secondary">GraphQL</li>
         <li className="badge badge-pill my-0 mx-1 badge-secondary">Apollo</li>
         <li className="badge badge-pill my-0 mx-1 badge-secondary">CLI</li>
         <li className="badge badge-pill my-0 mx-1 badge-secondary">System Design</li>
         <li className="badge badge-pill my-0 mx-1 badge-secondary">Annotation System</li>
         <li className="badge badge-pill my-0 mx-1 badge-secondary">Agile</li>
         <li className="badge badge-pill my-0 mx-1 badge-secondary">Flask</li>
         <li className="badge badge-pill my-0 mx-1 badge-secondary">Django</li>
         </div>
       <hr></hr>
      </div>
    </div>
  );
}
