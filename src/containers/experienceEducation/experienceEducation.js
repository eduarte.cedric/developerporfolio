import React, {Fragment} from "react";
import ExperienceCard from "../../components/experienceCard/ExperienceCard";
import { experienceEducation } from "../../portfolio";
import { Fade } from "react-reveal";
import { VerticalTimeline, VerticalTimelineElement}  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import { MdSchool, MdWork, MdFitnessCenter} from 'react-icons/md';
import { GoLaw } from "react-icons/go";


const WorkCSS = {
  background: '#2F65BC', color: '#fff'
}

export default function ExperienceEducation() {

    return (
    <React.Fragment>
      <h1 className="project-title text-center">Experience & Education</h1>
      <div key="id" id="expEdu" className="col-lg-6 float-right">
      {experienceEducation.expEdu.map(expEd =>{
      return (   
      <div id="experience">
            
                <Fade bottom duration={2000} distance="30px">
                 <VerticalTimeline
                 layout={'1-column'}
                 >
                  <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date={expEd.date}
                    iconStyle={{ background: '#2F65BC', color: '#fff' }}
                    icon={expEd.icon}
  
                    
                  >
                      <h4 className="vertical-timeline-element-title">{expEd.jobTitle}</h4>
                      <h5 className="vertical-timeline-element-subtitle">{expEd.company}</h5>
                      <span>{expEd.location}</span>
                      <p>
                        {expEd.description}
                      </p>
                    
                  </VerticalTimelineElement>
                </VerticalTimeline>
                </Fade>
            </div>
        );

     })}
      </div>  
    </React.Fragment>
      
    );
}
