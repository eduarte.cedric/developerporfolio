import React from "react";
import "./WorkExperience.css";
import ExperienceCard from "../../components/experienceCard/ExperienceCard";
import { workExperiences } from "../../portfolio";
import { Fade } from "react-reveal";
import { VerticalTimeline, VerticalTimelineElement}  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import { MdSchool, MdWork, MdFitnessCenter} from 'react-icons/md';
import { GoLaw } from "react-icons/go";


export default function WorkExperience() {
    if(workExperiences.viewExperiences){
        return (
    <React.Fragment>        
        <div id="experience">
            <h1 className="project-title text-center">Experience & Education</h1>
            <div className="row">
                     <div className="col-lg-6">   
                        <Fade bottom duration={2000} distance="30px">
          
                         <VerticalTimeline
                    

                         >
                          <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            date="July 2016 - Present"
                            iconStyle={{ background: '#2F65BC', color: '#fff' }}
                            icon={<MdWork/>}
                          >
                              <h4 className="vertical-timeline-element-title">Senior Logistics Associate</h4>
                              <h5 className="vertical-timeline-element-subtitle">Kambi Philippines Inc.</h5>
                              <span>Makati City, Philippines</span>
                              <p>
                                
                              </p>
                            
                          </VerticalTimelineElement>

                          <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            date="March 2020 - November 2020"
                            iconStyle={{ background: '#2F65BC', color: '#fff' }}
                            icon={<MdWork/>}
                          >
                              <h4 className="vertical-timeline-element-title">Junior Project Manager/Project Assistant</h4>
                              <h5 className="vertical-timeline-element-subtitle">Zuitt/Tokyo University</h5>
                              <span>Makati City, Philippines</span>
                              <p>
                                
                              </p>
                            
                          </VerticalTimelineElement>
                          
                          <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            date="February 2020 - April 2020"
                            iconStyle={{ background: '#2F65BC', color: '#fff' }}
                            icon={<MdWork/>}
                          >
                              <h4 className="vertical-timeline-element-title">Digitizer/Quality Controller</h4>
                              <h5 className="vertical-timeline-element-subtitle">Zuitt/Tokyo University</h5>
                              <span>Makati City, Philippines</span>
                              <p>
                                
                              </p>
                            
                          </VerticalTimelineElement>

                          <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            date="June 2016 - July 2016"
                            iconStyle={{ background: '#2F65BC', color: '#fff' }}
                            icon={<MdWork/>}
                          >
                              <h4 className="vertical-timeline-element-title">Second Line Support Officer - Sports Operations</h4>
                              <h5 className="vertical-timeline-element-subtitle">Everlounge Inc.</h5>
                              <span>Makati City, Philippines</span>
                              <p>
                                
                              </p>
                             
                          </VerticalTimelineElement>


                          <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            date="May 2013 - January 2016"
                            iconStyle={{ background: '#2F65BC', color: '#fff' }}
                            icon={<MdWork/>}
                          >
                              <h4 className="vertical-timeline-element-title">Assistant Bookmaker</h4>
                              <h5 className="vertical-timeline-element-subtitle">Yew Tree Services Inc.</h5>
                              <span>Makati City, Philippines</span>
                              <p>
                                
                              </p>
                             
                          </VerticalTimelineElement>

                            </VerticalTimeline>

                        </Fade>
                     </div>
                     <div className="col-lg-6">   
                        <Fade bottom duration={2000} distance="30px">
          
                         <VerticalTimeline

                         >
                          <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            date="2019"
                            iconStyle={{ background: '#2FBC57', color: '#fff' }}
                            icon={<MdSchool/>}
                           
                          >
                              <h4 className="vertical-timeline-element-title">Bootcamper</h4>
                              <h5 className="vertical-timeline-element-subtitle">Zuitt Web Development Bootcamp</h5>
                              <span>Makati City, Philippines</span>  
                              <p>
                                
                              </p>
                            </VerticalTimelineElement>

                          <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            date="July 2015 - July 2017"
                            iconStyle={{ background: '#2FBC57', color: '#fff' }}
                            icon={<GoLaw/>}
                           
                          >
                              <h4 className="vertical-timeline-element-title">Student</h4>
                              <h5 className="vertical-timeline-element-subtitle">San Beda College of Law</h5>
                              <span>Manila, Philippines</span>  
                              <p>
                                
                              </p>
                            </VerticalTimelineElement>
                          <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            date="May - July 2014 "
                            iconStyle={{ background: '#2FBC57', color: '#fff' }}
                            icon={<MdFitnessCenter/>}
                           
                          >
                              <h4 className="vertical-timeline-element-title">Student</h4>
                              <h5 className="vertical-timeline-element-subtitle">Gold's Fitness Institute</h5>
                              <span>Manila, Philippines</span>  
                              <p>
                                
                              </p>
                            </VerticalTimelineElement>
                          <VerticalTimelineElement
                            className="vertical-timeline-element--work"
                            date="2009-2013"
                            iconStyle={{ background: '#2FBC57', color: '#fff' }}
                            icon={<MdSchool/>}
                           
                          >
                              <h4 className="vertical-timeline-element-title">Graduate - AB Political Science</h4>
                              <h5 className="vertical-timeline-element-subtitle">Rizal Technological University </h5>
                              <span>Mandaluyong City, Philippines</span>  
                              <p>
                                
                              </p>
                            </VerticalTimelineElement>

                            </VerticalTimeline>

                        </Fade>
                     </div>
            </div>
        </div>

 
    </React.Fragment>        


        );
    }
    return null;
}
